const { event } = require('../models')
const { body, validationResult } = require('express-validator');

module.exports = {
    create: async (req,res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }

            const data = await event.create({
                name: req.body.name,
                description: req.body.description,
                artist_name: req.body.artist_name,
                location: req.body.location,
                start_date: req.body.start_date,
                end_date: req.body.end_date,
                stock_quantity: req.body.stock_quantity,
                price: req.body.price,
                is_active: true,
                image: req.file.path
            });

            return res.json({
                data: data
            })
        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    }
}