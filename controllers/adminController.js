const { admin } = require('../models')
const bcrypt = require('bcrypt');
const { body, validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

module.exports = {
    create: async (req,res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }

            const password = bcrypt.hashSync(req.body.password, 10)
            const data = await admin.create({
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                email: req.body.email,
                password: password,
                phone_no: req.body.phone_no
            });

            return res.json({
                data: data
            })
        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    login: async (req, res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }

            const user = await admin.findOne({ where : {
                email: req.body.email
            }})

            if(!user) {
                return res.json({
                    message : "Admin not exists!"
                })
            }

            if (bcrypt.compareSync(req.body.password, user.password)) {
                const token = jwt.sign(
                  { id: user.id },
                  'secret',
                  { expiresIn: '6h' }
                )
          
                return res.status(200).json({
                    token: token
                })
              }
            
            return res.json({
                message : "Wrong Password!"
            })
            
        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    getProfile: async(req, res) => {
        try {
            const user = await admin.findOne({ where : {
                id: res.user.id
            }})
            
            return res.json({
                data : user
            })
            
        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    }
}