const express = require('express')
const router = express.Router()
const adminController = require('./controllers/adminController');
const eventController = require('./controllers/eventController');
const { body } = require('express-validator');
const checkToken = require('./middleware/checkToken');
const uploadFile = require('./middleware/uploadFile');

router.post('/admin/create',body('email').isEmail(), body('first_name').notEmpty(),adminController.create);
router.post('/admin/login',body('email').isEmail().notEmpty(), body('password').notEmpty() ,adminController.login);
router.get('/admin/profile', checkToken, adminController.getProfile);
router.post('/event/create',
    uploadFile.single('photo'),
    body('name').notEmpty(),
    body('artist_name').notEmpty(),
    body('location').notEmpty(),
    body('start_date').notEmpty(),
    body('end_date').notEmpty(),
    body('stock_quantity').notEmpty(),
    body('price').notEmpty(),
    eventController.create
)

module.exports = router