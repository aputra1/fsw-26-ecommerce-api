'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('orders', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      booking_code: {
        type: Sequelize.STRING
      },
      type: {
        type: Sequelize.STRING
      },
      total_quantity: {
        type: Sequelize.INTEGER
      },
      sub_total: {
        type: Sequelize.FLOAT
      },
      total: {
        type: Sequelize.FLOAT
      },
      billing_key: {
        type: Sequelize.STRING
      },
      billing_no: {
        type: Sequelize.STRING
      },
      billing_type: {
        type: Sequelize.STRING
      },
      is_paid: {
        type: Sequelize.BOOLEAN
      },
      is_cancel: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('orders');
  }
};