const multer = require('multer')

const imageFilter = (req, file, cb) => {
    if (!file.originalname.match(/\.(JPG|jpg|jpeg|png|gif|PNG)$/)) {
      return cb(new Error('Only image files are allowed!'), false)
    }
    cb(null, true)
}

const uploadFile = multer({
    fileFilter: imageFilter,
    storage: multer.diskStorage({
      destination: function (req, file, cb) {
        cb(null, './uploads')
      },
      filename: function (req, file, cb) {
        cb(null, `${Date.now().toString()}.${file.originalname.split('.').pop()}`)
      }
    })
})

module.exports = uploadFile